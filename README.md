Psionics is a Minecraft modpack I created and maintain mainly for me,
but only because I am currently the only regular player.
It is centered around Vazkii's Psi mod.

I created this GitLab project purely so I could make use of the issue tracker
so that other people playing my pack can alert me to bugs and issues that
I may not have noticed myself.

The info on the pack can be found here.
http://www.atlauncher.com/pack/Psionics

The ATLauncher can be downloaded here.
https://www.atlauncher.com/downloads

The pack-code for Psionics is "Psionics" (without quotes)